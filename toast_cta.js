

    jQuery(document).ready(function($) {

    var flag = 0;
    var myTimeOut = null;

    show_div();
    myTimeOut = setTimeout(hide_div, 20000);

    function show_div() {
        $('#block-toast-cta-output-block').stop(true).animate({
            'margin-bottom': 20,
            'opacity': '1'
        }, {
            queue: false,
            duration: 2000
        });
        flag = 1;
    };

    function hide_div() {
        $('#block-toast-cta-output-block').stop(true).animate({
            'margin-bottom': -70,
            'opacity': '0'
        }, {
            queue: false,
            duration: 2000
        });
        flag = 0;
    };

    var myTimeOut;

    $("#block-toast-cta-output-block").mouseout(function () {
        myTimeOut = setTimeout(hide_div, 1000)
    });

    $("#block-toast-cta-output-block").mouseover(function () {
        clearTimeout(myTimeOut);
    });

    });
